import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, AlertController, ToastController } from '@ionic/angular';
import { RoomsService } from '../../services/romms-service.service';
import { Router } from '@angular/router';
import { IRoom } from '../../models/iroom.model';
import { WorkingRoom } from 'src/app/models/workingRoom.model';
import { MeetingRoom } from 'src/app/models/meetingRoom.model';

@Component({
  selector: 'app-modal-edit-room',
  templateUrl: './modal-edit-room.component.html',
  styleUrls: ['./modal-edit-room.component.scss'],
})
export class ModalEditRoomComponent {

  room: IRoom = this.navParams.get('room');
  newName: string;
  newDescription: string;

  newFilas: number;
  newColumnas: number;

  newCapacidad: number;

  constructor(private rs: RoomsService, public modalController: ModalController,
    public alertController: AlertController, private navParams: NavParams,
    private router: Router, public toastController: ToastController) {

  }

  onClickDeleteRoom() {
    this.presentAlertDeleteRoom();
  }

  async presentAlertDeleteRoom() {
    const alert = await this.alertController.create({
      header: 'Confirmar acción',
      subHeader: '¿Está seguro que desea eliminar la sala?',
      message: 'Esta acción no puede deshacerse',
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      { text: 'Confirmar', handler: () => { this.rs.deleteRoom(this.room); this.dismiss(); } }]
    });

    await alert.present();
  }

  onClickModifyWorkingRoom() {
    let r = this.room as WorkingRoom;
    if (this.newName == undefined)
      this.newName = r.name;
    if (this.newDescription == undefined)
      this.newDescription = r.descripcion;
    if (this.newFilas == undefined)
      this.newFilas = r.rows;
    if (this.newColumnas == undefined)
      this.newColumnas = r.cols;
    this.rs.modifyWorkingRoom(r.name, this.newName, this.newDescription, this.newFilas, this.newColumnas);

    this.presentToastRoomModified();
    this.dismiss();
  }

  onClickModifyMeetingRoom() {
    let r = this.room as MeetingRoom;
    if (this.newName == undefined)
      this.newName = r.name;
    if (this.newDescription == undefined)
      this.newDescription = r.descripcion;
    if (this.newCapacidad == undefined)
      this.newCapacidad = r.capacidad;
    this.rs.modifyMeetingRoom(r.name, this.newName, this.newName, this.newCapacidad);

    this.presentToastRoomModified();
    this.dismiss();
  }

  async presentToastRoomModified() {
    const toast = await this.toastController.create({
      message: 'La sala ' + this.newName + ' se ha modificado correcamente.',
      color: 'success',
      duration: 2000
    });
    toast.present();
  }

  async presentAlertModifyRoom() {
    const alert = await this.alertController.create({
      header: 'Confirmar acción',
      subHeader: '¿Está seguro que desea modificar la sala?',
      message: 'Esta acción elimina todas las asignaciones de escritorio',
      buttons: [{ text: 'Cancelar', role: 'cancel' },
      { text: 'Confirmar', handler: () => { this.dismiss(); this.presentToastRoomModified(); } }]
    });

    await alert.present();
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
