import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { RoomsService } from '../../services/romms-service.service';

@Component({
  selector: 'app-modal-create-room',
  templateUrl: './modal-create-room.component.html',
  styleUrls: ['./modal-create-room.component.scss'],
})
export class ModalCreateRoomComponent implements OnInit {

  @Input() ubicacionPrevia;
  @Input() plantaPrevia;

  nombreSala: string;
  descripcionSala: string;
  ubicacionSala: string;
  plantaSala: number;
  tipoSala: string;

  filas: number;
  columnas: number;

  capacidad: number;

  constructor(private rs: RoomsService, public modalController: ModalController, public toastController: ToastController) { }

  ngOnInit() {
    
  }

  onClickCreateWorkingRoom() {
    this.rs.addWorkingRoom(this.nombreSala, this.descripcionSala, this.ubicacionSala, this.plantaSala, this.filas, this.columnas);
    console.log('Sala trabajo: ', this.filas, this.columnas);

    this.presentToastRoomAdded();
    this.resetSelectedItems();
    this.dismiss();
  }

  onClickCreateMeetingRoom() {
    this.rs.addMeetingRoom(this.nombreSala, this.descripcionSala, this.ubicacionSala, this.plantaSala, this.capacidad);
    console.log('Sala reuniones:', this.capacidad);
    
    this.presentToastRoomAdded();
    this.resetSelectedItems();
    this.dismiss();
  }

  async presentToastRoomAdded() {
    const toast = await this.toastController.create({
      message: 'La sala ' + this.nombreSala + ' se ha creado correcamente.',
      color: 'success',
      duration: 2000
    });
    toast.present();
  }

  resetSelectedItems() {
    if (this.ubicacionPrevia !== null && this.ubicacionPrevia !== undefined)
      this.rs.selectUbicacion(this.ubicacionPrevia.id);
    if (this.plantaPrevia !== null && this.plantaPrevia !== undefined)
      this.rs.selectPlanta(this.plantaPrevia)
    console.log('Ubicacion vuelta: ', this.rs.selectedUbicacion);
    console.log('Planta vuelta: ', this.rs.selectedPlanta);
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
