import { Component } from '@angular/core';
import { RoomsService } from 'src/app/services/romms-service.service';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { Reserva } from 'src/app/models/reserva.model';
import { Reservable } from 'src/app/models/reservable.model';
import { DateSelector } from 'src/app/models/date.model';

@Component({
  selector: 'app-modal-reserve-desk',
  templateUrl: './modal-reserve-desk.component.html',
  styleUrls: ['./modal-reserve-desk.component.scss'],
})
export class ModalReserveDeskComponent {
  fecha: DateSelector;
  horas: {
    lower: number,
    upper: number
  }
  reservable: Reservable = this.navParams.get('reservable');
  red:boolean;

  constructor(private rs: RoomsService, public modalController: ModalController,
    public alertController: AlertController, private navParams: NavParams) { 
      this.fecha = new DateSelector();
      this.fecha.setFecha(this.rs.selectedDate);
      this.horas = {
        lower: 8,
        upper: 20
      }
  }

  fixRange(){
    if(this.horas.lower === this.horas.upper){
      if(this.horas.lower < 20) 
        this.horas.upper++; 
      else 
        this.horas.lower--;
    }
    this.fecha.update();
  }

  checkValues() {
    this.checkRed("");
    if (this.reservable.checkRange(this.fecha.fecha, this.horas.lower, this.horas.upper)) {
      this.presentAlertBusyRoom();
      return;
    } 
    this.reservable.addReserve(new Reserva(this.fecha.fecha, this.horas.lower, this.horas.upper, "userX"));
    this.rs.updateDesksInRoom();
    this.dismiss();
  }

  async presentAlertBusyRoom() {
    const alert = await this.alertController.create({
      header: 'Ese periodo colisiona con uno ya existente',
      subHeader: 'No se ha podido registrar la reserva reservar',
      message: 'Asegurate de seleccionar un horario disponible',
      buttons: [{ text: 'Entendido', role: 'cancel' }]
    });

    await alert.present();
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  checkRed(e){
    this.fixRange();
    this.red = this.reservable.checkRange(this.fecha.fecha, this.horas.lower, this.horas.upper);
  }

  ionChangeDate(e) {
    this.checkRed(e);
  }



  

}
