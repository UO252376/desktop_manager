import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ModalRenderRoomComponent } from './modal-render-room.component';
import { DeskComponent } from 'src/app/components/desk-component/desk-component.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        FormsModule
    ],
    declarations:[
        ModalRenderRoomComponent,
        DeskComponent
    ],
    exports:[
        ModalRenderRoomComponent
    ]
})
export class ModalRenderRoomComponentModule {}