import { Component, OnInit } from '@angular/core';
import { RoomsService } from 'src/app/services/romms-service.service';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { DateSelector } from 'src/app/models/date.model';

@Component({
  selector: 'modal-render-room',
  templateUrl: './modal-render-room.component.html',
  styleUrls: ['./modal-render-room.component.scss'],
})
export class ModalRenderRoomComponent {
  
  modName: string;
  modDescription: string;
  fecha:DateSelector;
  horas: {
    lower: number;
    upper: number;
  };

  constructor(private rs: RoomsService, public modalController: ModalController,
    public alertController: AlertController, private navParams: NavParams) { 
      this.horas = {
        lower: 8,
        upper: 20
      }
      this.fecha = new DateSelector();
      this.fecha.setFecha(this.rs.selectedDate);
    this.rs.selectHourByIndex(this.fecha.fecha, this.horas.lower, this.horas.upper);
  }

  ionChange(event){
    this.rs.selectHourByIndex(this.fecha.fecha, this.horas.lower, this.horas.upper);
  }

  ionChangeDate(e) {
    this.fecha.update();
    this.ionChange(e);
  }

}
