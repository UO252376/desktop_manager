const { Pool } = require('pg')

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'desk',
    password: 'Desk12',
    port: 5432,
});

//OFICINAS

const query_get_oficinas = 'SELECT * FROM "TOFICINAS"';
const query_get_oficina_id = 'SELECT * FROM "TOFICINAS" where "CN_OFICINA" = $1';
const query_insert_oficina = 'INSERT INTO "TOFICINAS" ( "CN_OFICINA", "TE_OFICINA") VALUES ($1, $2)';
const query_update_oficina_id = 'UPDATE "TOFICINAS" SET "TE_OFICINA"= $2 where "CN_OFICINA" = $1';
const query_delete_oficina_id = 'DELETE FROM "TOFICINAS " WHERE "CN_OFICINA" = $1';

const getOficinas = (request, response) => {
  pool.query(query_get_oficinas, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getOficinaById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(query_get_oficina_id, [id], (error, result) => {
    if (error) {
      throw error
    }
    response.status(200).json(result.rows)
  })

}

const createOficina = (request, response) => {
  const { id, text } = request.body

  pool.query(query_insert_oficina, [id, text], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send('Oficina añadida con id: ' + results.insertId)
  })
}

const updateOficina = (request, response) => {
  const id = parseInt(request.params.id)
  const { text } = request.body

  pool.query(query_update_oficina_id,
    [id, text],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send('Oficina modificada con id: ' + results.insertId)
    }
  )
}

const deleteOficina = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query(query_delete_oficina_id, [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send('Oficina eliminada')
  })
}

//PLANTAS

const query_get_plantas = 'SELECT * FROM "TPLANTAS"';
const query_get_planta_id = 'SELECT * FROM "TPLANTAS" where "CN_PLANTA" = $1';
const query_insert_planta = 'INSERT INTO "TPLANTAS" ( "CN_PLANTA", "TE_DESCRIPCION") VALUES ($1, $2)';
const query_update_planta_id = 'UPDATE "TPLANTAS" SET "TE_DESCRIPCION"= $2 where "CN_PLANTA" = $1';
const query_delete_planta_id = 'DELETE FROM "TPLANTAS " WHERE "CN_PLANTA" = $1';

const getPlantas = (request, response) => {
  pool.query(query_get_plantas, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getPlantaById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(query_get_planta_id, [id], (error, result) => {
    if (error) {
      throw error
    }
    response.status(200).json(result.rows)
  })

}

const createPlanta = (request, response) => {
  const { id, descripcion } = request.body

  pool.query(query_insert_planta, [id, descripcion], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send('Planta añadida con id: ' + results.insertId)
  })
}

const updatePlanta = (request, response) => {
  const id = parseInt(request.params.id)
  const { descripcion } = request.body

  pool.query(query_update_planta_id,
    [id, descripcion],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send('Planta modificada con id: ' + results.insertId)
    }
  )
}

const deletePlanta = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query(query_delete_planta_id, [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send('Planta eliminada')
  })
}

//SALAS

const query_get_salas = 'SELECT * FROM "TSALAS"';
const query_get_sala_id = 'SELECT * FROM "TSALAS" where "CN_ID" = $1';
const query_insert_sala = 'INSERT INTO "TSALAS" ( "CN_ID", "TE_NAME", "CN_PLANTA", "CN_OFICINA", "NU_FILA, "NU_COLUMNA") VALUES ($1, $2, $3, $4, $5, $6)';
const query_update_sala_id = 'UPDATE "TSALAS" SET "TE_NAME"= $2, "CN_PLANTA"= $3, "CN_OFICINA"= $4, "NU_FILA"= $5, "NU_COLUMNA"= $6 where "CN_SALA" = $1';
const query_delete_sala_id = 'DELETE FROM "TSALAS " WHERE "CN_ID" = $1';

const getSalas = (request, response) => {
  pool.query(query_get_salas, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getSalaById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(query_get_sala_id, [id], (error, result) => {
    if (error) {
      throw error
    }
    response.status(200).json(result.rows)
  })

}

const createSala = (request, response) => {
  const { id, nombre, planta, oficina, filas, columnas } = request.body

  pool.query(query_insert_sala, [id, nombre, planta, oficina, filas, columnas], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send('Sala añadida con id: ' + results.insertId)
  })
}

const updateSala = (request, response) => {
  const id = parseInt(request.params.id)
  const { nombre, planta, oficina, filas, columnas } = request.body

  pool.query(query_update_sala_id,
    [id, nombre, planta, oficina, filas, columnas],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send('Sala modificada con id: ' + results.insertId)
    }
  )
}

const deleteSala = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query(query_delete_sala_id, [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send('Sala eliminada')
  })
}

//SITIOS

const query_get_sitios = 'SELECT * FROM "TSITIOS"';
const query_get_sitio_id = 'SELECT * FROM "TSITIOS" where "CN_SITIO" = $1';
const query_insert_sitio = 'INSERT INTO "TSITIOS" ( "CN_SITIO", "CN_FILA, "CN_COLUMNA", "CN_SALA") VALUES ($1, $2, $3, $4)';
const query_update_sitio_id = 'UPDATE "TSITIOS" SET "CN_FILA"= $2, "CN_COLUMNA"= $3, "CN_SALA"= $4 where "CN_sitio" = $1';
const query_delete_sitio_id = 'DELETE FROM "TSITIOS " WHERE "CN_SITIO" = $1';

const getSitios = (request, response) => {
  pool.query(query_get_sitios, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getSitioById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(query_get_sitio_id, [id], (error, result) => {
    if (error) {
      throw error
    }
    response.status(200).json(result.rows)
  })

}

const createSitio = (request, response) => {
  const { id, fila, columna, sala } = request.body

  pool.query(query_insert_sitio, [id, fila, columna, sala], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send('Sitio añadido con id: ' + results.insertId)
  })
}

const updateSitio = (request, response) => {
  const id = parseInt(request.params.id)
  const { fila, columna, salas } = request.body

  pool.query(query_update_sitio_id,
    [id, fila, columna, sala],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send('Sitio modificado con id: ' + results.insertId)
    }
  )
}

const deleteSitio = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query(query_delete_sitio_id, [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send('Sitio eliminado')
  })
}

//RESERVAS

const query_get_reservas = 'SELECT * FROM "TRESERVAS"';
const query_get_reserva_id = 'SELECT * FROM "TRESERVAS" where "CN_ID" = $1';
const query_insert_reserva = 'INSERT INTO "TRESERVAS" ( "CN_ID", "CN_USUARIO, "CN_SITIO", "FE_FECHA", "CN_HORAINICIO", "CN_HORAFIN") VALUES ($1, $2, $3, $4, $5, $6)';
const query_update_reserva_id = 'UPDATE "TRESERVAS" SET "CN_USUARIO"= $2, "CN_SITIO"= $3, "FE_FECHA"= $4, "CN_HORAINICIO"= $5, "CN_HORAFIN"= $6 where "CN_ID" = $1';
const query_delete_reserva_id = 'DELETE FROM "TRESERVAS " WHERE "CN_ID" = $1';

const getReservas = (request, response) => {
  pool.query(query_get_reservas, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getReservaById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(query_get_reserva_id, [id], (error, result) => {
    if (error) {
      throw error
    }
    response.status(200).json(result.rows)
  })

}

const createReserva = (request, response) => {
  const { id, usuario, sitio, fecha, horaInicio, horaFin } = request.body

  pool.query(query_insert_reserva, [id, usuario, sitio, fecha, horaInicio, horaFin], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send('Reserva añadida con id: ' + results.insertId)
  })
}

const updateReserva = (request, response) => {
  const id = parseInt(request.params.id)
  const { usuario, sitio, fecha, horaInicio, horaFin } = request.body

  pool.query(query_update_reserva_id,
    [id, usuario, sitio, fecha, horaInicio, horaFin],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send('Reserva modificada con id: ' + results.insertId)
    }
  )
}

const deleteReserva = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query(query_delete_reserva_id, [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send('Reserva eliminada')
  })
}

//USUARIOS

const query_get_usuarios = 'SELECT * FROM "TUSUARIOS"';
const query_get_usuario_id = 'SELECT * FROM "TUSUARIOS" where "ID" = $1';
const query_insert_usuario = 'INSERT INTO "TUSUARIOS" ( "ID", "NOMBRE, "PRIMER_APELLIDO", "SEGUNDO_APELLIDO", "CANAL", "ROL", "USER_NAME", "EMAIL", "PASSWORD") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)';
const query_update_usuario_id = 'UPDATE "TUSUARIOS" SET "NOMBRE"= $2, "PRIMER_APELLIDO"= $3, "SEGUNDO_APELLIDO"= $4, "CANAL"= $5, "ROL"= $6, "USER_NAME"= $7, "EMAIL"= $8, "PASSWORD"= $9 where "ID" = $1';
const query_delete_usuario_id = 'DELETE FROM "TUSUARIOS " WHERE "CN_usuario" = $1';

const getUsuarios = (request, response) => {
  pool.query(query_get_usuarios, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getUsuarioById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(query_get_usuario_id, [id], (error, result) => {
    if (error) {
      throw error
    }
    response.status(200).json(result.rows)
  })

}

const createUsuario = (request, response) => {
  const { id, nombre, primerApellido, segundoApellido, canal, rol, userName, email, password } = request.body

  pool.query(query_insert_usuario, [id, nombre, primerApellido, segundoApellido, canal, rol, userName, email, password], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send('Usuario añadido con id: ' + results.insertId)
  })
}

const updateUsuario = (request, response) => {
  const id = parseInt(request.params.id)
  const { nombre, primerApellido, segundoApellido, canal, rol, userName, email, password } = request.body

  pool.query(query_update_usuario_id,
    [id, nombre, primerApellido, segundoApellido, canal, rol, userName, email, password],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send('Usuario modificado con id: ' + results.insertId)
    }
  )
}

const deleteUsuario = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query(query_delete_usuario_id, [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send('Usuario eliminado')
  })
}

module.exports = { 
  getOficinaById, getOficinas, createOficina, updateOficina, deleteOficina,
  getPlantaById, getPlantas, createPlanta, updatePlanta, deletePlanta,
  getSalaById, getSalas, createSala, updateSala, deleteSala,
  getSitioById, getSitios, createSitio, updateSitio, deleteSitio,
  getReservaById, getReservas, createReserva, updateReserva, deleteReserva,
  getUsuarioById, getUsuarios, createUsuario, updateUsuario, deleteUsuario,

}