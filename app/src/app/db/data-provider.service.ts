import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataProviderService {

  ruta: string;

  constructor(public http: HttpClient) {
    this.ruta = "http://localhost:3000";
  }

  //OFICINAS
  getOficinas() {
    return this.http.get(this.ruta + '/oficinas');
  }

  getOficinaById(id) {
    return this.http.get(this.ruta + '/oficinas/' + id);
  }

  createOficina(id, texto) {
    return this.http.post(this.ruta + '/oficinas', { "id": id, "text": texto });
  }

  updateOficina(id, texto) {
    return this.http.put(this.ruta + '/oficinas/' + id, { "text": texto });
  }

  deleteOficinas(id) {
    return this.http.delete(this.ruta + '/oficinas/' + id);
  }

  //PLANTAS
  getPlantas() {
    return this.http.get(this.ruta + '/plantas');
  }

  getPlantaById(id) {
    return this.http.get(this.ruta + '/plantas/' + id);
  }

  createPlanta(id, descripcion) {
    return this.http.post(this.ruta + '/plantas', { "id": id, "descripcion": descripcion });
  }

  updatePlanta(id, descripcion) {
    return this.http.put(this.ruta + '/plantas/' + id, { "descripcion": descripcion });
  }

  deletePlanta(id) {
    return this.http.delete(this.ruta + '/plantas/' + id);
  }

  //SALAS
  getSalas() {
    return this.http.get(this.ruta + '/salas');
  }

  getSalaById(id) {
    return this.http.get(this.ruta + '/salas/' + id);
  }

  createSala(id, nombre, planta, oficina, filas, columnas) {
    return this.http.post(this.ruta + '/salas', { "id": id, "nombre": nombre, "planta": planta, "oficina": oficina, "filas": filas, "columnas": columnas });
  }

  updateSala(id, nombre, planta, oficina, filas, columnas) {
    return this.http.put(this.ruta + '/salas/' + id, { "nombre": nombre, "planta": planta, "oficina": oficina, "filas": filas, "columnas": columnas });
  }

  deleteSala(id) {
    return this.http.delete(this.ruta + '/salas/' + id);
  }

  //SITIOS
  getSitios() {
    return this.http.get(this.ruta + '/sitios');
  }

  getSitioById(id) {
    return this.http.get(this.ruta + '/sitios/' + id);
  }

  createSitio(id, fila, columna, sala) {
    return this.http.post(this.ruta + '/sitios', { "id": id, "fila": fila, "columna": columna, "sala": sala });
  }

  updateSitio(id, fila, columna, sala) {
    return this.http.put(this.ruta + '/sitios/' + id, { "fila": fila, "columna": columna, "sala": sala });
  }

  deleteSitio(id) {
    return this.http.delete(this.ruta + '/sitios/' + id);
  }

  //RESERVA
  getReservas() {
    return this.http.get(this.ruta + '/reservas');
  }

  getReservaById(id) {
    return this.http.get(this.ruta + '/reservas/' + id);
  }

  createReserva(id, usuario, sitio, fecha, horaInicio, horaFin) {
    return this.http.post(this.ruta + '/reservas', { "id": id, "usuario": usuario, "sitio": sitio, "fecha": fecha, "horaInicio": horaInicio, "horaFin": horaFin });
  }

  updateReserva(id, usuario, sitio, fecha, horaInicio, horaFin) {
    return this.http.put(this.ruta + '/reservas/' + id, { "usuario": usuario, "sitio": sitio, "fecha": fecha, "horaInicio": horaInicio, "horaFin": horaFin });
  }

  deleteReserva(id) {
    return this.http.delete(this.ruta + '/reservas/' + id);
  }
}

