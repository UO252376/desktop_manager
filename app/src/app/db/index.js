const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./queries')
const port = 3000

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
// Configurar cabeceras y cors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

//OFICINAS
app.get('/oficinas', db.getOficinas)
app.post('/oficinas', db.createOficina)
app.get('/oficinas/:id', db.getOficinaById)
app.put('/oficinas/:id', db.updateOficina)
app.delete('/oficinas/:id', db.deleteOficina)

//PLANTAS
app.get('/plantas', db.getPlantas)
app.post('/plantas', db.createPlanta)
app.get('/plantas/:id', db.getPlantaById)
app.put('/plantas/:id', db.updatePlanta)
app.delete('/plantas/:id', db.deletePlanta)

//SALAS
app.get('/salas', db.getSalas)
app.post('/salas', db.createSala)
app.get('/salas/:id', db.getSalaById)
app.put('/salas/:id', db.updateSala)
app.delete('/salas/:id', db.deleteSala)

//SITIO
app.get('/sitios', db.getSitios)
app.post('/sitios', db.createSitio)
app.get('/sitios/:id', db.getSitioById)
app.put('/sitios/:id', db.updateSitio)
app.delete('/sitios/:id', db.deleteSitio)


//RESERVAS
app.get('/reservas', db.getReservas)
app.post('/reservas', db.createReserva)
app.get('/reservas/:id', db.getReservaById)
app.put('/reservas/:id', db.updateReserva)
app.delete('/reservas/:id', db.deleteReserva)

/*
//USUARIOS
app.get('/usuarios', db.getusuarios)
app.post('/usuarios', db.createUsuario)
app.get('/usuarios/:id', db.getUsuarioById)
app.put('/usuarios/:id', db.updateUsuario)
app.delete('/usuarios/:id', db.deleteUsuario)
*/
app.listen(port, () => {
  console.log('App running on port: ', port)
})