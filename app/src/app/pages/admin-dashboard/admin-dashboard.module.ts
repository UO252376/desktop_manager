import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AdminDashboardPage } from './admin-dashboard.page';
import { ModalEditRoomComponent } from 'src/app/modals/modal-edit-room/modal-edit-room.component';
import { ModalCreateRoomComponent } from 'src/app/modals/modal-create-room/modal-create-room.component';
import { RoomInMenuComponentModule } from 'src/app/components/room-in-menu/room-in-menu.module';
import { ModalRenderRoomComponent } from 'src/app/modals/modal-render-room/modal-render-room.component';
import { ModalRenderRoomComponentModule } from 'src/app/modals/modal-render-room/modal-render-room.module';
import { ModalReserveDeskComponent } from 'src/app/modals/modal-reserve-desk/modal-reserve-desk.component';

const routes: Routes = [
  {
    path: '',
    component: AdminDashboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    RoomInMenuComponentModule,
    ModalRenderRoomComponentModule
  ],
  entryComponents: [
    ModalCreateRoomComponent,
    ModalEditRoomComponent,
    ModalRenderRoomComponent,
    ModalReserveDeskComponent
  ],
  declarations: [
    AdminDashboardPage,
    ModalCreateRoomComponent,
    ModalEditRoomComponent
  ]
  
})
export class AdminDashboardPageModule {}
