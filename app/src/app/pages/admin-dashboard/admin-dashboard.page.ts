import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ModalController } from '@ionic/angular';
import { RoomsService } from 'src/app/services/romms-service.service';
import { ModalCreateRoomComponent } from 'src/app/modals/modal-create-room/modal-create-room.component';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.page.html',
  styleUrls: ['./admin-dashboard.page.scss'],
})
export class AdminDashboardPage implements OnInit {

  constructor(private auth: AuthenticationService, private rs: RoomsService, public modalController: ModalController) { }

  ngOnInit() {
  }

  signOut(){
    this.auth.signOut();
  }

  async presentModalCreateRoom() {
    console.log('Modal añadir salas');
    const modal = await this.modalController.create({
      component: ModalCreateRoomComponent,
      cssClass: "create-room-modal",
      componentProps: { roomService: this.rs, ubicacionPrevia: this.rs.selectedUbicacion,
        plantaPrevia: this.rs.selectedPlanta }
    });
    return await modal.present();
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
