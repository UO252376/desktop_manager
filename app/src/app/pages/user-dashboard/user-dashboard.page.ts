import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { RoomsService } from 'src/app/services/romms-service.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.page.html',
  styleUrls: ['./user-dashboard.page.scss'],
})
export class UserDashboardPage implements OnInit {

  constructor(private auth: AuthenticationService, private rs: RoomsService) { }

  ngOnInit() {
  }

  signOut(){
    this.auth.signOut();
  }

}
