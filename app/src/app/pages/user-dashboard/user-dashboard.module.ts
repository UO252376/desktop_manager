import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserDashboardPage } from './user-dashboard.page';
import { RoomInMenuComponentModule } from 'src/app/components/room-in-menu/room-in-menu.module';
import { ModalRenderRoomComponentModule } from 'src/app/modals/modal-render-room/modal-render-room.module';
import { ModalRenderRoomComponent } from 'src/app/modals/modal-render-room/modal-render-room.component';
import { ModalReserveDeskComponent } from 'src/app/modals/modal-reserve-desk/modal-reserve-desk.component';

const routes: Routes = [
  {
    path: '',
    component: UserDashboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    RoomInMenuComponentModule,
    ModalRenderRoomComponentModule
  ],
  entryComponents: [
    ModalRenderRoomComponent,
    ModalReserveDeskComponent
  ],
  declarations: [
    UserDashboardPage
  ]
})
export class UserDashboardPageModule {}
