import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { Ubicacion } from 'src/app/models/ubicacion.model';
import { WorkingRoom } from 'src/app/models/workingRoom.model';
import { IRoom } from 'src/app/models/iroom.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user = {
    username: '',
    password: ''
  };
  
  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  signIn() {
    this.auth.signIn(this.user).subscribe(user => {
      console.log('after login: ', user)
      let role = user['role'];
      if(role=='ADMIN'){
        this.router.navigateByUrl('/admin-dashboard');
      } else if(role=='USER'){
        this.router.navigateByUrl('/user-dashboard');
      }
        });
   
  }
}
