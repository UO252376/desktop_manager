import { TestBed } from '@angular/core/testing';

import { RoomsService} from './romms-service.service';

describe('RommsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoomsService = TestBed.get(RoomsService);
    expect(service).toBeTruthy();
  });
});
