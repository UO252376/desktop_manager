import { Injectable, ɵisDefaultChangeDetectionStrategy } from '@angular/core';
import { IRoom } from '../models/iroom.model';
import { WorkingRoom } from '../models/workingRoom.model';
import { MeetingRoom } from '../models/meetingRoom.model';
import { Ubicacion } from '../models/ubicacion.model';
import { DataProviderService } from '../db/data-provider.service';
import { DateSelector } from '../models/date.model';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  ubicaciones: Array<Ubicacion>;
  oficinas;
  selectedUbicacion: Ubicacion;
  selectedPlanta: number;
  selectedRoom: IRoom;
  user: string;
  admin: boolean;

  horas: Array<string>;
  selectedDate: Date;
  selectedHour: number;
  hastaSelectedHour: number;

  constructor(private proveedor: DataProviderService) {
    this.ubicaciones = new Array<Ubicacion>();

    this.showOficina();


    this.ubicaciones.push(new Ubicacion("Oviedo", new Array<Array<IRoom>>(), 0, 2));
    this.ubicaciones[0].addRoom(0, new WorkingRoom("Trabajo 1", "Sala de comerciales", 10, 4));
    this.ubicaciones[0].addRoom(0, new WorkingRoom("Trabajo 2", "Sala de mantenimiento del Principado", 8, 4));
    this.ubicaciones[0].addRoom(0, new MeetingRoom("Reuniones 0.3", "Sala de reuniones con calefacción", 4));
    this.ubicaciones[0].addRoom(0, new WorkingRoom("Trabajo 1", "Sala de comerciales", 10, 4));
    this.ubicaciones[0].addRoom(0, new WorkingRoom("Trabajo 2", "Sala de mantenimiento del Principado", 8, 4));
    this.ubicaciones[0].addRoom(0, new MeetingRoom("Reuniones 0.3", "Sala de reuniones con calefacción", 4));
    this.ubicaciones[0].addRoom(0, new WorkingRoom("Trabajo 1", "Sala de comerciales", 10, 4));
    this.ubicaciones[0].addRoom(0, new WorkingRoom("Trabajo 2", "Sala de mantenimiento del Principado", 8, 4));
    this.ubicaciones[0].addRoom(0, new MeetingRoom("Reuniones 0.3", "Sala de reuniones con calefacción", 4));
    this.ubicaciones[0].addRoom(1, new MeetingRoom("Reuniones 1.1", "Sala de reuniones insonorizada", 20));
    this.ubicaciones[0].addRoom(1, new MeetingRoom("Reuniones 1.2", "Sala de reuniones de directiva", 8));
    this.selectedUbicacion = null;
    this.initHours();
  }

  showOficina() {
    this.proveedor.getOficinaById("1").subscribe(
      (data) => { console.log(data); } //data[0].TE_OFICINA -> Para coger el nombre
    );
  }

  initHours() {
    this.selectedRoom = null;
    this.horas = new Array<string>(12);
    this.selectedDate = new Date();
    for (let i = 0; i < this.horas.length; i++) {
      this.horas[i] = i < 2 ? "0" : "";
      this.horas[i] += (8 + i) + ":00";
    }
  }

  changeSelectedHour(fecha: Date, hour:string, hastaHour:string){
    let d = 0, h=0;
    for(let i=0;i<this.horas.length;i++){
      if(this.horas[i] === hour){
        d = i;
      } else if (this.horas[i] === hour) {
        h = i
      }
    }
    this.selectHourByIndex(fecha, d,h);
  }

  getSelectedHour() {
    return this.horas[this.selectedHour - 8];
  }
  getHastaSelectedHour() {
    return this.horas[this.hastaSelectedHour - 8];
  }

  selectHourByIndex (fecha:Date, d: number, h:number) {
    this.selectedDate = fecha === undefined ? new Date() : fecha;
    if (h > d){
      this.selectedHour = d;
      this.hastaSelectedHour = h;
    } else {
      this.selectedHour = h;
      this.hastaSelectedHour = d;
    }
    this.updateDesksInRoom();
  }

  updateDesksInRoom(){
    if(this.selectedRoom === undefined || this.selectedRoom === null) return;
    this.selectedHour===undefined ? this.selectHourByIndex(this.selectedDate, 0, this.horas.length-1) 
      : this.selectedRoom.checkDeskStates(this.selectedDate, this.selectedHour, this.hastaSelectedHour);
  }

  getRoom(planta: number, i: number) {
    return this.selectedUbicacion.getRoom(planta, i);
  }

  getRoomsForPlanta() {
    return this.selectedUbicacion.getPlanta(this.selectedPlanta);
  }



  selectPlanta(planta: number) {
    this.selectedPlanta = planta;
  }

  getPlantas() {
    return this.selectedUbicacion.getPlantas();
  }

  addWorkingRoom(name: string, descripcion: string, ubicacion: string, planta: number, filas: number, columnas: number) {
    this.getUbicacionById(ubicacion).getPlanta(planta).push(new WorkingRoom(name, descripcion, filas, columnas));
  }

  addMeetingRoom(name: string, descripcion: string, ubicacion: string, planta: number, capacidad: number) {
    this.getUbicacionById(ubicacion).getPlanta(planta).push(new MeetingRoom(name, descripcion, capacidad));
  }

  modifyWorkingRoom(name: string, newName: string, newDescription: string, newFilas: number, newColumnas: number) {
    let wr = this.getRoomsForPlanta().find(r => r.name == name) as WorkingRoom;
    let index = this.getRoomsForPlanta().indexOf(wr);
    wr.name = newName;
    wr.descripcion = newDescription;
    wr.rows = newFilas;
    wr.cols = newColumnas;
    wr.initDesks();
    this.getRoomsForPlanta().splice(index, 1, wr);

  }

  modifyMeetingRoom(name: string, newName: string, newDescription: string, newCapacidad: number) {
    let mr = this.getRoomsForPlanta().find(r => r.name == name) as MeetingRoom;
    let index = this.getRoomsForPlanta().indexOf(mr);
    mr.name = newName;
    mr.descripcion = newDescription;
    mr.capacidad = newCapacidad;
    this.getRoomsForPlanta().splice(index, 1, mr);
  }

  deleteRoom(r: IRoom) {
    this.selectedUbicacion.deleteRoom(r);
  }

  selectRoom(r: IRoom) {
    this.selectedRoom = r;
  }

  deselectRoom() {
    this.selectedRoom = null;
  }

  addUbicacion(u: Ubicacion) {
    this.ubicaciones.push(u);
  }
  selectUbicacion(id: string) {
    this.selectedUbicacion = this.getUbicacionById(id);
  }
  deselectUbicacion() {
    this.selectedUbicacion = null;
  }
  getUbicacionById(id: string) {
    for (let u of this.ubicaciones) {
      if (u.id === id) {
        return u;
      }
    }
  }

  setUser(user: string) {
    this.admin = user === "admin" ? true : false;
    this.user = user;
  }


}