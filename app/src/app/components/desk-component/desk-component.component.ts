import { Component, Input } from '@angular/core';
import { Desk } from '../../models/desk.model';
import { ModalController } from '@ionic/angular';
import { ModalRenderRoomComponent } from 'src/app/modals/modal-render-room/modal-render-room.component';
import { ModalReserveDeskComponent } from 'src/app/modals/modal-reserve-desk/modal-reserve-desk.component';

@Component({
  selector: 'desk-component',
  templateUrl: './desk-component.component.html',
  styleUrls: ['./desk-component.component.scss'],
})
export class DeskComponent {

  @Input() desk: Desk;

  constructor(public modalController: ModalController) { }

  async reservarSitio(){
    const modal = await this.modalController.create({
      component: ModalReserveDeskComponent,
      componentProps: {
        reservable: this.desk
      }
    });
    return await modal.present();
  }

  

}
