import { NgModule } from "@angular/core";
import { RoomInMenuComponent } from './room-in-menu.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ModalReserveDeskComponent } from 'src/app/modals/modal-reserve-desk/modal-reserve-desk.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations:[
        RoomInMenuComponent,
        ModalReserveDeskComponent,
    ],entryComponents:[
        ModalReserveDeskComponent,
    ],
    exports:[
        RoomInMenuComponent
    ]
})
export class RoomInMenuComponentModule {}