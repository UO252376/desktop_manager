import { Component, Input } from '@angular/core';
import { ModalEditRoomComponent } from '../../modals/modal-edit-room/modal-edit-room.component';
import { ModalController } from '@ionic/angular';
import { RoomsService } from '../../services/romms-service.service';
import { Router } from '@angular/router';
import { IRoom } from '../../models/iroom.model';
import { ModalRenderRoomComponent } from 'src/app/modals/modal-render-room/modal-render-room.component';
import { MeetingRoom } from 'src/app/models/meetingRoom.model';
import { ModalReserveDeskComponent } from 'src/app/modals/modal-reserve-desk/modal-reserve-desk.component';

@Component({
  selector: 'room-in-menu',
  templateUrl: './room-in-menu.component.html',
  styleUrls: ['./room-in-menu.component.scss'],
})
export class RoomInMenuComponent{
  
  @Input() room:IRoom;
  @Input() editable: boolean;

  constructor(private rs: RoomsService, public modalController: ModalController, private router: Router) {
  }

  async viewRoom(){
    this.rs.selectRoom(this.room);
    const modal = await this.modalController.create({
      component: ModalRenderRoomComponent, 
      componentProps: {room: this.room},
      cssClass: "modal-fullscreen"
    });
    return await modal.present();
  }

  async presentModalEditRoom() {
    const modal = await this.modalController.create({
      component: ModalEditRoomComponent,
      componentProps: { room: this.room}
    });
    return await modal.present();
  }

  async presentModalReserveRoom() {
    const modal = await this.modalController.create({
      component: ModalReserveDeskComponent,
      componentProps: { reservable: this.room as MeetingRoom}
    });
    return await modal.present();
  }
}
