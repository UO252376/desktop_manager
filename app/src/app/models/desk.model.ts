import { Time } from '@angular/common';
import { Reserva } from './reserva.model';
import { Reservable } from './reservable.model';

export class Desk extends Reservable{
    row: number;
    col: number;


    constructor(row: number, col: number) {
        super()
        this.row = row;
        this.col = col;
    }

    toString(){
        return "Mesa " + this.row + ", " + this.col;
    }

}