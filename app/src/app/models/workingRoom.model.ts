import { IRoom } from './iroom.model';
import { Desk } from './desk.model';

export class WorkingRoom extends IRoom {
    desks: Array<Array<Desk>>;
    rows: number;
    cols: number;

    constructor(name: string, descripcion: string, rows: number, cols: number) {
        super(name, descripcion, "trabajo");
        this.rows = rows;
        this.cols = cols;
        this.initDesks();
    }

    initDesks() {
        this.desks = new Array<Array<Desk>>();
        for (let i = 0; i < this.rows; i++) {
            this.desks[i] = new Array<Desk>();
            for (let j = 0; j < this.cols; j++) {
                this.desks[i][j] = new Desk(i, j);
            }
        }
    }

    checkDeskStates(fecha: Date, fromHour: number, untilHour: number) {
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.cols; j++) {
                this.desks[i][j].setRange(this.desks[i][j].checkRange(fecha, fromHour, untilHour));
                this.desks[i][j].checkFull(fecha);
            }
        }
    }

}