export class DateSelector {

    fecha: Date;
    selectorFecha: string;
    public minDate: string;
    public maxDate: string;

    constructor(){
        this.fecha = new Date();
        this.selectorFecha = this.getMinDate();
        this.minDate = this.getMinDate();
        this.maxDate = this.getMaxDate();
    }

    setFecha(fecha:Date){
        this.fecha.setFullYear(fecha.getFullYear());
        this.fecha.setMonth(fecha.getMonth());
        this.fecha.setDate(fecha.getDate());
        this.fecha=fecha;
        this.selectorFecha = this.toSelector(fecha);
    }

    update(){
        let tokens = this.selectorFecha.split("T")[0].split("-");
        this.fecha.setFullYear(+tokens[0]);
        this.fecha.setMonth(+tokens[1]-1);
        this.fecha.setDate(+tokens[2]);
    }

    getMinDate(): string{
        let fecha = new Date();
        return "" + fecha.getFullYear() + "-" + (fecha.getMonth()+1) + "-" + (fecha.getDate() < 10 ? "0" + fecha.getDate() : fecha.getDate());
    }

    getMaxDate(): string {
        let fecha = new Date();
        return "" + (fecha.getFullYear()+10) + "-" + (fecha.getUTCMonth()+1) + "-" +  (fecha.getDate() < 10 ? "0" + fecha.getDate() : fecha.getDate());
    }

    toSelector(fecha:Date){
        return "" + fecha.getFullYear() + "-" + (fecha.getMonth()+1) + "-" + (fecha.getDate() < 10 ? "0" + fecha.getDate() : fecha.getDate());
    }
}