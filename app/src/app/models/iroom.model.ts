export abstract class IRoom {
    name: string;
    descripcion: string;
    type: string;
    

    constructor(name: string, descripcion: string, type: string) {
        this.name = name;
        this.descripcion = descripcion;
        this.type=type;
    }

    abstract checkDeskStates(fecha: Date, hour:number, untilHour:number);
}