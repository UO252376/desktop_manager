import { Reserva } from './reserva.model';
export abstract class Reservable {

    
    full: boolean;
    current: boolean;
    reservas: Array<Reserva>;

    constructor(){
        this.full = false;
        this.current = false;
        this.reservas = new Array<Reserva>();
    }
    
    addReserve(reserva:Reserva) {
        this.reservas.push(reserva);
    }

    checkRange(fecha: Date, desde: number, hasta: number): boolean{
        let value= false;
        for (let i = 0; i < this.reservas.length; i++) {
            if (this.reservas[i].collisions(fecha, desde, hasta) ) {
                value = true;
            }
        }
        return value;
    }

    setRange(value: boolean){
        this.current = value;
    }

    checkFull(fecha:Date) {
        let aux = 0;
        this.reservas.forEach( r => { aux+= r.compareDate(fecha)=== 0 ? r.getLength(): 0; });
        this.full = aux == 20-8;
    }

    book(user: string, from: number, to: number) {
        this.reservas.push(new Reserva(new Date(), from, to, user));
    }

    getReservasParaHoy(fecha:Date){
        let hoy = new Array<Reserva>();
        this.reservas.forEach(r => r.compareDate(fecha) === 0 ? hoy.push(r): "")
        
        return hoy;
    }

    abstract toString();
}