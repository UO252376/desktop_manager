import { IRoom } from './iroom.model';

export class Ubicacion {
    id: string;
    plantas: Array<Array<IRoom>>;
    plantasNegativas: number;
    numeroPlantas: number;

    constructor(id: string, plantas: Array<Array<IRoom>>, plantasNegativas: number, numeroPlantas: number){
        this.id = id;
        this.plantas=plantas;
        this.plantasNegativas = plantasNegativas;
        this.numeroPlantas = numeroPlantas;
    }

    getPlanta(i:number){
        if(this.plantas[this.plantasNegativas + Number(i)] == undefined){
            this.plantas[this.plantasNegativas + Number(i)] = new Array<IRoom>();
        }
        return this.plantas[this.plantasNegativas + Number(i)];
    }

    getPlantas(){
        var indices = new Array<String>();
        for(let i=0;i<this.numeroPlantas;i++){
            indices.push((i-this.plantasNegativas)+"");
        }
        return indices;
    }

    serialize(){
        return JSON.stringify(this);
    }

    static fromJson(json: string){
        const instance = JSON.parse(json);
        return new Ubicacion(instance.id, instance.plantas, instance.plantasNegativas, instance.numeroPlantas);
    }

    addRoom(planta:number, room:IRoom){
        if(this.plantas[planta+this.plantasNegativas]===null
            || this.plantas[planta+this.plantasNegativas]===undefined){
            this.plantas[planta+this.plantasNegativas] = new Array<IRoom>();
        }
        
        this.plantas[planta+this.plantasNegativas].push(room);
    }

    getRoom(planta:number, i:number){
        return this.plantas[planta][i];
    }

    deleteRoom(room:IRoom){
        for(let i=0;i<this.numeroPlantas;i++){
            for(let j=0;j<this.plantas[i].length;j++){
                if(this.plantas[i][j].name === room.name){
                    this.plantas[i].splice(j, 1);
                }
            }
        }
    }


}