export class Reserva {

    fecha: Date;
    desde: number;
    hasta: number;
    usuario: string;

    constructor(fecha: Date, desde: number, hasta: number, usuario: string){
        this.fecha = new Date(fecha);
        this.desde = desde;
        this.hasta = hasta;
        this.usuario = usuario;
    }

    compareDate(d2:Date): number{
        let d1 = this.fecha;
        if(d1.getFullYear() === d2.getFullYear()){
            if(d1.getMonth() === d2.getMonth()){
                if(d1.getDate() === d2.getDate()){
                    return 0;
                } 
                else return d1.getDate() >= d2.getDate() ? 1 : -1;
            } 
            else return d1.getMonth() >= d2.getMonth() ? 1 : -1; 
        } 
        else return d1.getFullYear() >= d2.getFullYear() ? 1 : -1; 
    }

    compare(aux:Reserva): number{
        if(this.compareDate(aux.fecha) === 0 && this.toString() == aux.toString()){
            return 0;
        }
        return aux.desde>this.desde ? -1 : 1;
    }

    collisions(fecha:Date, desde:number, hasta:number): boolean{
        if(this.compareDate(fecha) === 0){ // Mismo día
            if(desde >= this.desde && desde <  this.hasta) return true; // Comienza dentro
            if(hasta >  this.desde && hasta <= this.hasta) return true; // Acaba dentro
            if(desde <= this.desde && hasta >= this.hasta) return true; // Coinciden o abarca
            return false;
        }
        else return false;
    }

    getLength():number{
        return this.hasta - this.desde;
    }

    toString():string {
        let s = "";
        s += this.desde < 10 ? "0" + this.desde : this.desde;
        s +=":00 - "
        s += this.hasta < 10 ? "0" + this.hasta : this.hasta;
        s +=":00"
        return s;
    }
}