import { IRoom } from './iroom.model';
import { Reserva } from './reserva.model';
import { Reservable } from './reservable.model';

export class MeetingRoom extends Reservable implements IRoom  {
    
    name: string;
    descripcion: string;
    type: string;  
    capacidad: number;

    reservas: Array<Reserva>;

    current: boolean;

    constructor(name: string, descripcion: string, capacidad: number) {
        super();
        this.type = "reunion";
        this.name = name;
        this.descripcion = descripcion;
        this.capacidad = capacidad;
        this.reservas = new Array<Reserva>();
    }
    
    checkCurrent(fecha:Date, desde:number, hasta:number): boolean {
        this.current = this.checkRange(fecha, desde, hasta);
        return this.current;
    }

    checkDeskStates(fecha: Date, hour: number, untilHour: number) { }

    toString(){
        return "Sala " + this.name
    }
}