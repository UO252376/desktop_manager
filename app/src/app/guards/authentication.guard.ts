import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { take, map } from 'rxjs/operators'
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthenticationService, private alertCtrl: AlertController) { }

  canActivate(route: ActivatedRouteSnapshot) {
    const expectedRole = route.data.role;
    console.log('Expected role: ', expectedRole);

    return this.auth.user.pipe(
      take(1),
      map(user => {
        console.log('log: ', user);
        if (user) {
          let role = user['role'];

          if (expectedRole == role) {
            return true;
          }

        }

        this.showAlert();
        return this.router.parseUrl('/login');

      })
    )
  }

  async showAlert() {
    let alert = await this.alertCtrl.create({
      header: 'Cuenta no autorizada',
      message: 'No tienes permisos para ver esta página',
      buttons: ['OK']
    });
    alert.present();
  }


}
